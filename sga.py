#########################################################################################
# Exercício Prático 2 – Algoritmos Genéticos 
# IME 2022 
# Disciplina de IA:
#
# Autores: Antonio Horta
#          Ronaldo Goldschmidt
#########################################################################################

'''
Considere um algoritmo genético simples, desenvolvido para minimizar 
a função matemática x² onde (0 <= x <= 15). SGA é uma superclasse escrita
em python para servir de base para execução dos procedimentos necessarios
ao exercício e encontra-se disponível no arquivo sga.py no Classroom. 

Ela possui alguns parâmetros que podem ser alterados pelo usuário e pode
ser extendida para criação de novas classes e funções. 

Os parametros são:

bounds = [[.0, 15.0]] 				# define range for input 
n_iter = 50 						# define the total iterations
n_bits = 4 							# bits per variable
n_pop = 10 							# define the population size
r_cross = 0.6						# crossover rate
r_mut = 0.05						# mutation rate

Faça o que se pede:

a)	Familiarize-se com o algoritmo genético spa.py executando várias rodadas 
do mesmo com diferentes parâmetros e observando os resultados obtidos.

b)	Crie a classe SGA1 extendida da superclasse SGA e crie 
a função "def plot_ga(self):" que deve gerar, apresentar e/ou salvar um grafico 
png dos resultados obtidos dos melhores cromossomos durante as interações. A 
sugestão é de utilizar a biblioteca "matplotlib"

c)	Na classe SGA1 e crie a função *def save_ga(self):* que deve salvar em um 
arquivo .csv dos melhores resultados obtidos dos melhores cromossomos durante 
as interações.

d)	Em seguida, altere o SGA1 para a execução de R (1 <= R <= 20) rodadas 
consecutivas com o respectivo armazenamento e grafico dos melhores cromossomas 
a cada geração nas R rodadas (séries). Certifique-se que, a cada rodada, seja 
usada uma série aleatória diferente. 

e)	Extenda SGA1 para SGA2 e sobreponha a função "def objective(self, x):" 
para utilizar a Normalização Linear como técnica de aptidão.

normalização: f(x) = y = (ymax - ymin) * [ (x - xmin)/(xmax - xmin) ] + ymin

f)	Implemente o Elitismo acrescentando a função "def elitismo(self):" na 
classe SGA3 extendendida de SGA2.

g)	Execute 20 rodadas de cada SGA (SGA1, SGA2 e SGA3) e obtenha as curvas de 
desempenho (melhor por geração e média dos experimentos) dos algoritmos criados. 
Utilize k=1 no SGA3. Analise os resultados. São coerentes com o que se espera em 
termos teóricos? Justifique.

O trabalho a ser entregue deve conter:

1-	Listagem do dos códigos fonte das classes SGA1, SGA2 e SGA3.
2-	Gráfico das curvas de desempenho do SGA1, SGA2 e SGA3.
3-	Análise dos resultados.

'''

from numpy.random import randint
from numpy.random import rand

class SGA:

	def __init__(self, bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k):

		self.bounds = bounds 		# define range for input
		self.n_iter = n_inter 		# define the total iterations
		self.n_bits = n_bits		# bits per variable
		self.n_pop = n_pop			# define the population size
		self.r_cross = r_cross		# crossover rate
		self.r_mut = r_mut			# mutation rate
		self.k = k					# candidates 

	# objective function
	def objective(self, x):
		f_obj = x[0]**2
		return f_obj

	# decode bitstring to numbers
	def decode(self, bitstring):
		decoded = list()
		largest = 2**self.n_bits
		for i in range(len(self.bounds)):
			# extract the substring
			start, end = i * self.n_bits, (i * self.n_bits)+ self.n_bits
			substring = bitstring[start:end]
			# convert bitstring to a string of chars
			chars = ''.join([str(s) for s in substring])
			# convert string to integer
			integer = int(chars, 2)
			# scale integer to desired range
			value = self.bounds[i][0] + (integer/largest) * (self.bounds[i][1] - self.bounds[i][0])
			# store
			decoded.append(value)
		return decoded

	# tournament selection
	def selection(self, pop, scores):
		# first random selection
		selection_ix = randint(len(pop))
		for ix in randint(0, len(pop), self.k-1):
			# check if better (e.g. perform a tournament)
			if scores[ix] < scores[selection_ix]:
				selection_ix = ix
		return pop[selection_ix]

	# crossover two parents to create two children
	def crossover(self, p1, p2, r_cross):
		# children are copies of parents by default
		c1, c2 = p1.copy(), p2.copy()
		# check for recombination
		if rand() < r_cross:
			# select crossover point that is not on the end of the string
			pt = randint(1, len(p1)-2)
			# perform crossover
			c1 = p1[:pt] + p2[pt:]
			c2 = p2[:pt] + p1[pt:]
		return [c1, c2]

	# mutation operator
	def mutation(self, bitstring, r_mut):
		for i in range(len(bitstring)):
			# check for a mutation
			if rand() < r_mut:
				# flip the bit
				bitstring[i] = 1 - bitstring[i]

	# genetic algorithm
	def run(self):
		# initial population of random bitstring
		pop = [randint(0, 2, self.n_bits*len(self.bounds)).tolist() for _ in range(self.n_pop)]

		# keep track of best solution
		best, best_eval = 0, self.objective(self.decode(pop[0]))
	
		# enumerate generations
		for gen in range(self.n_iter):

			# decode population
			decoded = [self.decode(p) for p in pop]
	
			# evaluate all candidates in the population
			scores = [self.objective(d) for d in decoded]
	
			# check for new best solution
			for i in range(self.n_pop):
				if scores[i] < best_eval:
					best, best_eval = pop[i], scores[i]
					print('\n..... inter[%s]' % str(gen+1).zfill(4) )
					print("\tgen[%s], new best f(%s) = %f" % (str(gen).zfill(4),  decoded[i], scores[i]))
	
			# select parents
			selected = [self.selection(pop, scores) for _ in range(self.n_pop)]
	
			# create the next generation
			children = list()
			for i in range(0, self.n_pop, 2):
				# get selected parents in pairs
				p1, p2 = selected[i], selected[i+1]
	
				# crossover and mutation
				for c in self.crossover(p1, p2, self.r_cross):
					# mutation
					self.mutation(c, self.r_mut)
	
					# store for next generation
					children.append(c)
	
			# replace population
			pop = children

			if best_eval == 0:
				break
	
		return [best, best_eval]


class SGA1(SGA):
	def __init__(self, bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k):
		super().__init__(bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k)

	def plot_ga(self):
		pass

	def save_ga(self):
		pass


class SGA2(SGA1):
	def __init__(self, bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k):
		super().__init__(bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k)

	def objective(self, x):		
		return


class SGA3(SGA2):
	def __init__(self, bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k):
		super().__init__(bounds, n_inter, n_bits, n_pop, r_cross, r_mut, k)

	def elitismo(self):		
		pass


#############################################
# 
# perform the simple genetic algorithm search
#
#############################################

# define range for input
bounds = [[0.0, 15.0]]
# bounds = [[0.0, 15.0], [0.0, 15.0]]
# define the total iterations
n_iter = 50
# bits per variable
n_bits = 4
# define the population size
n_pop = 10
# crossover rate
r_cross = 0.6
# mutation rate
r_mut = 0.05
# candidates
k = 3

print(f'\n==[ SGA INTERACTIONS ]==============')
sga1 = SGA(bounds=bounds, n_inter=n_iter, n_bits=n_bits, n_pop=n_pop, r_cross=r_cross, r_mut=r_mut, k=k)
best, score = sga1.run()

decoded = sga1.decode(best)
print('\n==[ THE BEST ]======================\n\n\tf(%s) = %f\n\n' % (decoded, score))
