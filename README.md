# Exercício Prático 2 – Algoritmos Genéticos

Considere um algoritmo genético simples, desenvolvido para minimizar a função matemática x² onde (0 <= x <= 15). SGA é uma superclasse escrita em python para servir de base para execução dos procedimentos necessarios ao exercício e encontra-se disponível no arquivo sga.py no Classroom. Ela possui alguns parâmetros que podem ser alterados pelo usuário e pode ser extendida para criação de novas classes e funções. 

Os parametros são:

```
bounds = [[.0, 15.0]]               # define range for input
n_iter = 50                         # define the total iterations
n_bits = 4                          # bits per variable
n_pop = 10                          # define the population size
r_cross = 0.6                       # crossover rate
r_mut = 0.05                        # mutation rate
k = 3                               # candidates
```

## Faça o que se pede:

a)	Familiarize-se com o algoritmo genético spa.py executando várias rodadas do mesmo com diferentes parâmetros e observando os resultados obtidos.

b)	Crie a classe SGA1 extendida da superclasse SGA para a  e crie a função *def plot_ga(self):* que deve gerar, apresentar e/ou salvar um grafico png dos resultados obtidos dos melhores cromossomos durante as interações.

c)	Na classe SGA1 e crie a função *def save_ga(self):* que deve salvar salvar em um arquivo .csv dos melhores resultados obtidos dos melhores cromossomos durante as interações.

d)	Em seguida, altere o SGA1 para a execução de R (1 <= R <= 20) rodadas consecutivas com o respectivo armazenamento e grafico dos melhores cromossomas a cada geração nas R rodadas (séries). Certifique-se que, a cada rodada, seja usada uma série aleatória diferente. A 
sugestão é de utilizar a biblioteca "matplotlib".


e)	Extenda SGA1 para SGA2 e sobreponha a função *def objective(self, x):* para utilizar a Normalização Linear como técnica de aptidão.

*f(x) = y = (ymax - ymin) * [ (x - xmin)/(xmax - xmin) ] + ymin*

f)	Implemente o Elitismo acrescentando a função "def elitismo(self):" na classe SGA3 extendendida de SGA2.

g)	Execute 20 rodadas de cada SGA (SGA1, SGA2 e SGA3) e obtenha as curvas de desempenho (melhor por geração e média dos experimentos) dos algoritmos criados. Utilize k=1 no SGA3. Analise os resultados. São coerentes com o que se espera em termos teóricos? Justifique.

## O trabalho a ser entregue deve conter:

1-	Listagem do dos códigos fonte das classes SGA1, SGA2 e SGA3.

2-	Gráfico das curvas de desempenho do SGA1, SGA2 e SGA3.

3-	Análise dos resultados.

# Instalação e execução

```
sudo apt install python3.8-venv
sudo apt install python3-virtualenv
pip3 install virtualenv
python3 -m venv ./myenv
source ./myenv/bin/activate
python sga.py
```
